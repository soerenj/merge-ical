<?php
/*
 * merge calendar (ical) files from offline computer (upload)
 *
 * https://gitlab.com/soerenj/merge-ical
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation.  Please see LICENSE.txt at the top level of
 * the source code distribution for details.
 */


//WARNING: Do NOT put sensitiv Data in this file, or otherwise delete the following block:
if(isset($_GET['sourcecode_self'])){ //print out own Source-Programm-Code (incl. Passworts, etc... )
    header('Content-Description: File Transfer');
    header('Content-Type: application/x-php');//header('Content-Type: application/octet-stream');
    header("Last-Modified: " . gmdate("D, d M Y H:i:s",filemtime(substr($_SERVER['SCRIPT_NAME'],1))) . " GMT");
    header('Content-Disposition: attachment; filename=' . substr($_SERVER['SCRIPT_NAME'],1));
    echo file_get_contents(substr($_SERVER['SCRIPT_NAME'],1));
    exit;
}


$notes = ''; //personal notes on page
/*e.g. personal note :
    <span role="note complementary">This Web-Service is only <b>until</b> </span><span id="year"><b>2021</b>-11-01 ⏱</span> available<span style="color:#555555">. 
<script language="javascript"> var d = new Date(); var n = d.getFullYear(); var m = d.getMonth();
if(n=="2021")document.getElementById("year").style.fontSize="18pt";
if(n=="2021" && (m==8 || m==9 || m==09 || m==10 ||  m==11))document.getElementById("year").style.color="red"
</script></span></span>
*/


if(!isset($_FILES['file'])){
echo '<html><head>
<title>merge multiple iCal files (upload)</title>
<style type="text/css">input{ display:block; }</style>
</head><body>';
}

if(!isset($_FILES['file'])) 	echo "<h1>Merge ical Files (for upload offline ical files)</h1>";
//tabs
if(!isset($_FILES['file']) && file_exists('ical-merge-upload.php')) echo '
<main>
  change to <a href="ical-merge.php">online</a> Url/Adress
  <section>
';



if(!isset($_FILES['file'])){
	echo $notes;
	echo "<form method='post' enctype='multipart/form-data' role='application'>
	<input type='file' name='file[]' multiple='multiple' placeholder='http://example.com/calendar1.ical' />
	<input type='file' name='file[]' multiple='multiple' placeholder='http://example.com/calendar1.ical' />
	<noscript>
	    <input type='file' multiple='multiple' name='file[]' />
    	<input type='file' multiple='multiple' name='file[]' />
    	<input type='file' multiple='multiple' name='file[]' />
    	<input type='file' multiple='multiple' name='file[]' />
    	<input type='file' multiple='multiple' name='file[]' />
    	<input type='file' multiple='multiple' name='file[]' /><br>
    	<br>maximal 8 fields in no JavaScript Mode
	</noscript>
	<button type='button' onclick='javascript:appendNew()' >add more files</button>
	<input type='submit' value='merge' />
	<script language='javascript' type='text/javascript'>
	function appendNew(){
	var node = document.createElement('input'); node.setAttribute('multiple','multiple'); node.setAttribute('name','file[]'); node.setAttribute('type','file');  node.setAttribute('size','50'); 
	document.getElementsByTagName('form')[0].appendChild(node);
	}
	</script>
	</form>
	<br><details lang='en'><summary style='cursor:pointer'>About</summary><br>
Online Service to merge offline/upload ical-files.<br/>
It just put the ical files all together in one ( BEGIN:VCALENDAR -> END:VCALENDAR ).<br/>
Nothing else will be done. Support multiple Files. SourceCode: <a href='https://gitlab.com/soerenj/merge-ical' rel='noreferrer' title='@git'>[1]</a> <a href='".$_SERVER['SCRIPT_NAME']."?sourcecode_self=1' title='direct download'>[2]</a></details>
	";
echo '<script language="javascript">
  var language = window.navigator.userLanguage || window.navigator.language;
  //translation to german
  if(language.substring(0,2)=="de"){
    
    var newItem = document.createElement("H1");
    document.getElementsByTagName("h1")[0].textContent="Zusammenführen von .ical";
    var p = document.createElement("p");
    p.textContent="für hochgeladene .ical / .ics Dateien ( Kalender-Dateien offline )";
    p.style="display:block;font-size:12pt;font-weight: normal;margin-top:0pt;";
    var ref = document.getElementsByTagName("section")[0].firstChild;
    ref.parentNode.insertBefore(p,ref.nextSibling);
    var span = document.querySelector("span[role=\'note complementary\']");
    span.innerHTML = span.innerHTML.replace(/This Web-Service is only/,"Dieser Webservice ist nur erreichbar bis: &nbsp;");//"Zusammenführen von calendar ical Adressen (für online ical dateien / urls)"; 
    span.innerHTML = span.innerHTML.replace(/until/,"");
    span.nextSibling.nextSibling.textContent = span.nextSibling.nextSibling.textContent.replace(/available/,""); 
    document.querySelector("input[value=merge]").value="zusammenführen";
    document.querySelector("button[onclick=\'javascript:appendNew()\']").textContent="weiteres Feld hinzufügen";

    //document.querySelector("input[type=submit]").value="create ical";
    //document.body.innerHTML = document.body.innerHTML.replace(/Kalender auslesen: experimentell/g, "Calendar export: experimentell");
  }
</script>';
}else{


	header('Content-type: text/calendar; charset=utf-8');
	header('Content-Disposition: inline; filename=calendar.ics');

    $merge_ical = new merge_ical();
    foreach($_FILES['file']['tmp_name'] as $file){
      if(strlen($file)<5 && $file=='')continue;
      //if(substr($file,0,1)=='/' || substr($file,0,1)=='\\')die(strip_tags($file)." no allowed "); 
	  if($file!='') $merge_ical->echo_file($file);	
	}
	echo "END:VCALENDAR";

}

class merge_ical{
public $iCalHeadWasWritten = false;

public function echo_file($file){
	ini_set('default_socket_timeout', 3);
	$context = stream_context_create( array(
	  'http'=>array(
		'timeout' => 3
	  )
	));
	$fp = fopen($file, "r",FALSE,$context);  
    if($fp===false){ echo "error in reading ". strip_tags($file); return;}
	$fileLines = array();
    $this->iCalAfterHead = false;
	while (!feof($fp)){
	  $this->printOut(fgets($fp));
	} 
	$this->iCalHeadWasWritten = true;
	fclose($fp);
}

public function printOut($out){
  if(strpos($out,'END:VCALENDAR')!==FALSE)return;
  if($this->iCalHeadWasWritten){ 
    if( !$this->iCalAfterHead && strpos($out,'BEGIN:VEVENT')!==FALSE) $this->iCalAfterHead=true;
    if( !$this->iCalAfterHead ) return;
  }
  echo $out;
}

}
 
