# merge-ical

## =>[ test it now ](http://www.multiple-ical.de/ical-merge.php) <= online web-Service until Nov. 2021 available

## Merge multiple ical Calendar files
ical-Calendar files: merge online-urls of upload offline files and merge them

![screenshot (old)](screenshot.png)

Two Version:
* merge online files (__URL__ / URI)
* merge offline files (__upload__)
 
Features:
* supports more then two files,...
* just a stupid merging: no checking, or special actions / no convert done with the data
* for PHP (Webserver) and Web-Browser
* reuse-able url (only on online-files) like: ``http://example.com/merge.php?url%5B%5D=http%3A%2F%2Fserver1.com%2file1.ics&url%5B%5D=http%3A%2F%2Fserver2.com%2Ffile2.ics&``

